var useGoogleApi = process.argv[2] && process.argv[2] == 'useGoogleApi=true';
var csv = require('fast-csv');
var jsonfile = require('jsonfile');

// https://developers.google.com/maps/documentation/timezone
var google_api = require('./google.json');
var request = require('request');

if (!useGoogleApi) {
    var tzwhere = require('tzwhere');
    tzwhere.init();
}

var csvData = [];
var result = {};

csv.fromPath('us-area-code-geo.csv')
    .on('data', function(data){
        csvData.push({
            code: data[0],
            lat: data[1],
            long: data[2]
        });
    })
    .on('end', function(){
        
        console.log("done with US area codes ...");

        csv.fromPath('ca-area-code-geo.csv')
        .on('data', function(data){
            csvData.push({
                code: data[0],
                lat: data[1],
                long: data[2]
            });
        })
        .on('end', function(){
            console.log("done with CA area codes ...");
            processCSV(csvData);
        });

    });

function processCSV(data) {
    for (var i = 0; i < data.length; i++) {
        if (!useGoogleApi) {
            result[data[i].code] = tzwhere.tzNameAt(data[i].lat, data[i].long);
            // If all results reached then save file
            if (Object.keys(result).length == csvData.length) {
                jsonfile.writeFileSync('result.json', result, {spaces: 1});
            }
        } else {
            doGoogleApiRequest(data[i]);  
        }
    }
}

function doGoogleApiRequest(data) {
    var requestUrl = google_api.base_url.concat('?location=').concat(data.lat).concat(',').concat(data.long)
                    .concat('&timestamp=').concat(Math.floor(Date.now()/1000))
                    .concat('&key=').concat(google_api.api_key);
    request.get({
        uri: requestUrl,
        json: true
    }, 
    function(error,response,body) {
        console.log(requestUrl);
        console.log(body.timeZoneId);
        result[data.code] = body.timeZoneId || null;
        // If all results reached then save file
        if (Object.keys(result).length == csvData.length) {
            jsonfile.writeFileSync('result.json', result, {spaces: 1});
        }
    });
}